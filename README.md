A playground with [SnapKit](https://github.com/SnapKit/SnapKit).

To use the playground, open `SnapKit.xcworkspace` and build the framework once for the Simulator. Then, open `UBPlayground` in the project navigator and start coding.

## Adding external resources

Resources can be added by dragging them to the folder "Resources" under `UBPlayground`.

## TODO

- Add UBFoundationUI (-swift?)
